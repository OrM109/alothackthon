/**
 * Created by arolave on 26/06/2016.
 */
const MongoClient = require('mongodb'); // eslint-disable-line  import/no-extraneous-dependencies
const Promise = require('bluebird');

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/admin';

const db = () => MongoClient.connect(
              mongoUrl,
    {
        poolSize: process.env.MONGO_POOL_SIZE || 10,
        promiseLibrary: Promise,
        sslValidate: false,
    },
);
export default db();
