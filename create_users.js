var mongojs = require('mongojs');
var db = mongojs('hackthon_dev', ['users']);
const _ = require('lodash');



var createUserDoc = function (user, pass, name, age, cityName, level, description,
                             interests, gender, loc, contactInfo) {
    return {
        _id: new mongojs.ObjectId().toHexString(),
        user : user, 
        pass : pass, 
        name : name, 
        age : age, 
        cityName : cityName, 
        level : level, 
        description : description,  
        interests,
        gender,
        loc,
        contactInfo : contactInfo
    }
};

var addNewUser = function (user, pass, name, age, cityName, level, description,
                             interests, gender, loc, contactInfo) {
    db.users.insert(createUserDoc(user, pass, name, age, cityName, level, description,
                                     interests, gender, loc, contactInfo));
};

db.users.drop();
// Step 1: Add users.
addNewUser("lena", "a", "Lena", 13, "Holon", "low", "Kind", "Computers, Animals", "Female", [34.788781, 32.017815], 
            {"mobile" : "0525534355", "email" : "lena13@fds.com"});
addNewUser("nira", "a", "Nira", 15, "BatYam", "low", "DuobleKind", "Animals", "Female", [ 34.748055, 32.020567], 
            {"mobile" : "0535787452", "email" : "Nir13@fds.com"});

addNewUser("avi", "a", "Avi", 9, "TelAviv", "High", "Nice", "Flowers", "Male", [ 34.774876, 32.060596], 
            {"mobile" : "0525534366", "email" : "avi@fds.com"});
addNewUser("yosi", "a", "Yosi", 15, "TelAviv", "Medium", "Lovely", "Bags", "Male", [34.771647, 32.070997], 
            {"mobile" : "0525534377", "email" : "yosi@fds.com"});
addNewUser("eli", "a", "Eli", 10, "TelAviv", "Low", "Nice and Lovely", "Debags", "Male", [34.798589, 32.091360], 
            {"mobile" : "0584563111", "email" : "eli00@fds.com"});
addNewUser("hana", "a", "Hana", 18, "TelAviv", "Medium", "Lovely and Happy", "Butterflyes", "Female", [34.768478, 32.080896], 
            {"mobile" : "0559898320", "email" : "hani66@fds.com"});

addNewUser("meni", "a", "Meni", 11, "Haifa", "High", "Tall", "Cars", "Male", [34.983048, 32.824095], 
            {"mobile" : "0525534355", "email" : "meni@fds.com"});
addNewUser("moshe", "a", "Moshe", 21, "Haifa", "High", "Tall and Nice", "Girls", "Male", [34.987654, 32.811847], 
            {"mobile" : "0548434787", "email" : "moshe3@fds.com"});
addNewUser("josh", "a", "Josh", 8, "Tira", "Medium", "Tall and Kind", "Mouses", "Male", [34.973508, 32.767918], 
            {"mobile" : "0554852310", "email" : "joshi2001@fds.com"});

addNewUser("lena2", "a", "Lena2", 12, "Jerusalem", "low", "Cute", "Poker", "Female", [35.216888, 31.771139], 
            {"mobile" : "0525534355", "email" : "lena12@fds.com"});                        
addNewUser("jack", "a", "Jack", 22, "Jerusalem", "low", "Cute", "Swim", "Male", [35.213004, 31.790125], 
            {"mobile" : "0525500356", "email" : "jack22@fds.com"});
addNewUser("yosi", "1234", "Yosi", 22, "TelAviv", "Medium", "Lovely and Happy", "Butterflyes", "Female", [34.768478, 32.080896],
           {"mobile" : "0559898320", "email" : "hani66@fds.com"});

db.users.ensureIndex({
    loc: "2dsphere"
});