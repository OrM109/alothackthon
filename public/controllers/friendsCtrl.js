var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.controller('friendsCtrl', function ($scope, $http, $location, $rootScope) {

    $scope.search = function(){
        $http.get('/friends').success(function (res) {
            if (res) {
                $scope.users = res;
            } else {
                $location.path("/login");
            }
        });
    };

    $scope.selectUser = function (userId) {
        $location.path("/profile/" + userId);
    }

    $scope.search();
});