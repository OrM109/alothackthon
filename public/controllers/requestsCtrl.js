var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.controller('requestsCtrl', function ($scope, $http, $location, $rootScope) {

    $scope.search = function(){
        $http.get('/requests',).success(function (res) {
            if (res) {
                $scope.users = res;
            } else {
                $location.path("/login");
            }
        });
    };

    $scope.selectUser = function (userId) {
        $location.path("/profile/" + userId);
    }

    $scope.search();
});