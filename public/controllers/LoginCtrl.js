var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.controller('LoginCtrl', function($scope, $http, $location, $rootScope) {
    // var getPosition = function (options) {
    //     return new Promise(function (resolve, reject) {
    //         navigator.geolocation.getCurrentPosition(resolve, reject, options);
    //     });
    // }
    $scope.loginError = false;

    // var positionPromise = getPosition();
    $scope.login = function (){
        // positionPromise.then(function (p){
            $http.post("/login", {
                user: $scope.username,
                pass: $scope.password,
                // loc: [p.coords.longitude, p.coords.latitude]
                loc: [34.7844303, 32.072392]
            }).success(function(res){
                if(res) {
                    $rootScope.user = res;
                    $location.path("/home");
                }
            }).catch(function (err) {
                $scope.loginError = true;
            });
        // }).catch(function (err) {
        //     console.log("error get location");
        // });
    };
    $scope.signup = function(){
        $location.path("/signup");
    };
});