var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.controller('HomeCtrl', function ($scope, $http, $location) {
    $scope.radius = 5;
    $scope.gender = '';
    $scope.minAge = 1;
    $scope.maxAge = 100;

    $http.get('/online').success(function (res) {
        if (res) {
            $scope.onlineUsers = res;
        }
    });

    $scope.selectUser = function (userId) {
        $location.path("/profile/" + userId);
    }

    $scope.search = function(){
        $http.post('/search', {
            radios: Number($scope.radius),
            minAge: Number($scope.minAge),
            maxAge: Number($scope.maxAge),
            gender: $scope.gender
        }).success(function (res) {
            if (res && res.users) {
                $scope.users = res.users;
            } else {
                $location.path("/login");
            }
        });
    };

    $scope.search();
});