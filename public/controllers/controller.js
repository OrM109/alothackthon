// Create a module for the controllers
var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);
app.run(function () {
    var mdlUpgradeDom = false;
    setInterval(function() {
      if (mdlUpgradeDom) {
        componentHandler.upgradeDom();
        mdlUpgradeDom = false;
      }
    }, 200);

    var observer = new MutationObserver(function () {
      mdlUpgradeDom = true;
    });
    observer.observe(document.body, {
        childList: true,
        subtree: true
    });
    /* support <= IE 10
    angular.element(document).bind('DOMNodeInserted', function(e) {
        mdlUpgradeDom = true;
    });
    */
});

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/home', {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl'
        }).
        when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
        }).
        when('/signup', {
            templateUrl: 'views/signup.html',
            controller: 'SignupCtrl'
        }).
        when('/about', {
            templateUrl: 'views/about.html',
            controller: 'searchCtrl'
        }).
        when('/control', {
            templateUrl: 'views/control.html',
            controller: 'botCtrl'
        }).
        when('/requests', {
            templateUrl: 'views/friends.html',
            controller: 'requestsCtrl'
        }).
        when('/profile/:id', {
            templateUrl: 'views/profile.html',
            controller: 'ProfileCtrl'
        }).
        when('/friends', {
            templateUrl: 'views/friends.html',
            controller: 'friendsCtrl'
        }).
        when('/signout', {
            resolve: {
                signout: ['SignoutSrv', function (SignoutSrv) {
                    SignoutSrv();
                }]
            }
        }).
        otherwise({
            redirectTo: '/login'
        });
    }
]);
