var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.controller('SignupCtrl', function($scope, $http, $location, $rootScope) {
    $scope.signup = function (){
        var details = {
            user: $scope.username,
            pass: $scope.password,
            name: $scope.name,
            age: $scope.age,
            cityName: $scope.city,
            gender: $scope.gender,
            level: $scope.level,
            description: $scope.description,
            interests: $scope.interests,
            contactInfo: {
                email: $scope.email,
                phone: $scope.phone
            }
        }

        $http.post("/register", details).success(function(res){
            if(res.msg === details.user + ' successfully registered'){
                $rootScope.user = details.user;
                $location.path("/home");
            }
        });
    };
    $scope.login = function(){
        $location.path("/login");
    };
});