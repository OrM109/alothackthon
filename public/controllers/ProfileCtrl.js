var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.controller('ProfileCtrl', function($scope, $http, $location, $routeParams) {
    $http.get("/profile/"+$routeParams['id']).success(function(res){
        $scope.profile = res;
    });

    $http.get("/liked/"+$routeParams['id']).success(function(res){
        if(res && res.msg) {
            if (res) {
                $scope.liked = res.liked;
                $scope.likeStatus = res.msg;
            }
        }
    });
    
    $scope.like = function (){
        $http.get("/like/"+$routeParams['id']).success(function(res){
            if(res && res.msg) {
                $scope.liked = true;
                $scope.likeStatus = res.msg;
            }
        });
    }

    $scope.back = function(){
        $location.path("/home");
    }
});