function makeURL(data) {

    var link = "https://www.facebook.com/search/";
    link = link + "str/" + data.name + "/users-named/";

    if (data.gender == 0) {
        link = link + "males/";
    }

    if (data.gender == 1) {
        link = link + "females/";
    }
    for (var like of data.likes) {
        console.log(like);
        link = link + "str/" + like.name + "/pages-named/likers/";
    }

    if (data.age) {
        link = link + data.age + "/users-age/intersect/";
    }

    return link;
}
//https://www.facebook.com/search/str/Or/users-named/males/str/9GAG/pages-named/likers/26/users-age/intersect/
var user = {
    name: 'Gal',
    age: '26',
    gender: 1,
    //likes: [{ id: '123', name: '9GAG' }, { id: '444', name: '9GAG' }]
    likes: [{ id: '123', name: 'Workey - Your Career Navigator' }]
};

var user2 = {
    name: 'Or',
    age: '26',
    gender: 0,
    //likes: [{ id: '123', name: '9GAG' }, { id: '444', name: '9GAG' }]
    likes: [{ id: '123', name: '9GAG' }]
};


console.log(makeURL(user));
