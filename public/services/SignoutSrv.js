var app = app || angular.module('myApp', ['ngRoute', 'ngCookies']);

app.factory('SignoutSrv', function($location, $cookies, $http) {
    return function(){
        $http.post("/logout", {})
          .success(function(res){
            if (res) {
                console.log($cookies.get('connect.sid'));

                //not working
                // $cookies.remove('connect.sid');
                console.log($cookies.get('connect.sid'));
                $location.path('/login');
            }
        });
    };
});