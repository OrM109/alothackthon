const express = require('express');
const app = express();
const path = require('path');
const url = require('url');
const queryString = require('querystring');
// var mongodb = require('mongodb');
const mongojs = require('mongojs');
const db = mongojs(process.env.MONGODB_URI || 'hackthon_dev', ['users_dev']);
//const db = mongojs('mantz:mantz@ds131621.mlab.com:31621/heroku_d3zsb2m0', ['users_dev']);
const session = require('express-session');
const uuid = require('uuid');
const _ = require('lodash');

const bodyParser = require('body-parser');

app.set('trust proxy', 1); // trust first proxy

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(session({
//   secret: 'keyboard cat',
//   resave: false,
//   saveUninitialized: true,
//   cookie: { secure: true }
// }))
app.use(session({
                    genid: function (req) {
                        return uuid() // use UUIDs for session IDs
                    },
                    secret: 'keyboard cat'
                }))

// parse application/json
app.use(bodyParser.json());


const DEFAULT_LOC = [34.7874731, 32.071143];

var intervalActive = false;
var loc = {};

let sessionMap = {};

const getUser = function (session) {
    if (!session) {
        return;
    }

    return sessionMap[session];
};

app.post('/search', (req, res) => {
    const user = getUser(req.session.id);
    const body = req.body;

    if (!user) {
        res.json({ error: "user not exists" });
        return;
    }

    const { radios, minAge, maxAge, gender } = body;
    const { loc } = user;

    const q = getQueryForLocation(loc, radios);
    const q2 = { _id: { $ne: user._id } };
    const q3 = (minAge && maxAge) ? { age: { $gte: minAge, $lte: maxAge } } : {};
    const q4 = (gender) ? { gender } : {};

    db.users.find(_.assign(q, q2, q3, q4), (err, docs) => {
        if (err) {
            res.json(err);
            return err;
        }
        res.json({ users: docs });
    });
});

app.get('/profile/:id', (req, res) => {
    const _id = req.params.id;
    db.users.findOne({ _id }, { pass: 0, contactInfo: 0 }, (err, user) => {
        if (user) {
            res.json(user);
        } else {
            res.json({ msg: "user not exist" });
        }
    });
});

app.get('/contact/:id', (req, res) => {
    const _id = req.params.id;
    db.users.findOne({ _id }, { contactInfo: 1 }, (err, user) => {
        if (user) {
            res.json(user);
        } else {
            res.json({ msg: "user not exist" });
        }
    });
});

app.post('/logout', (req, res) => {
    const sessionId = req.session.id;
    delete sessionMap[sessionId];
    console.log(`sessionId removed ${sessionId}`);

    res.send(200);
});

app.post('/login', (req, res) => {
    const sessionId = req.session.id;
    const { user, pass } = req.body;
    let { loc } = req.body;

    if (!_.first(loc)) {
        loc = DEFAULT_LOC;
    }

    db.users.findOne({ user: _.toLower(user), pass }, (err, user) => {
        if (user) {
            //const loc = [32.0746808, 34.7899611];
            _.assign(user, { loc });

            db.users.update({ _id: user._id }, { $set: { loc } }, (err, docs) => { sessionMap[sessionId] = user; });
            console.log(`@New login: ${user.name} logged in from location ${loc}`)

            res.json(user);
        }
        else {
            res.status(403).send({ msg: "user not exists" });
        }
    });
});

app.get('/requests', (req, res) => {
    const sessionId = req.session.id;
    const user = sessionMap[sessionId];

    db.likes.find({ likedUser: user._id, isApproved: { $ne: true } }, (err, docs) => {
        if (!docs) {
            res.json({ msg: "no requests found" });
        } else {
            db.users.find({ _id: { $in: _.map(docs, 'userId') } },{ contactInfo: 0 }, (err, users) => {
                res.json(users);
            });
        }
    });
});

app.get('/friends', (req, res) => {
    const sessionId = req.session.id;
    const user = sessionMap[sessionId];

    
    db.friends.findOne({ userId: user._id }, (err, friends) => {
        if (!friends) {
            res.json({ msg: "no friends found" });
        }
        else {
            const friendsList = friends.friends;
            db.users.find({ _id: { $in: friendsList } }, (err, docs) => {
                res.json(docs);
            });
        }
    });
});

app.get('/online', (req, res) => {
    const users = _.map(sessionMap, '_id');

    res.json(users);
});

app.get('/liked/:id', (req, res) => {
    const user = getUser(req.session.id);

    if (!user) {
        res.json({ error: "user not exists" });
        return;
    }
    const likedUser = req.params.id;
    db.friends.findOne({ userId: user._id, friends: likedUser }, (err, match) => {
        if (match) {
            res.json({ friends: true, liked: true, msg: "already friends" });
        } else {
            db.likes.findOne({ userId: likedUser, likedUser: user._id }, (err, like) => {
                if (like) {
                    res.json({ friends: false, liked: false, msg: "approve friend request" });
                } else {
                     db.likes.findOne({ userId: user._id, likedUser }, (err, like) => {
                        if (!like) {
                            res.json({ friends: false, liked: false, msg: "send friend request" });
                        } else {
                            res.json({ friends: false, liked: true, msg: "friend request sent" });
                        }
                    });
                }
            });
        }
    });
});



app.get('/like/:id', (req, res) => {
    const user = getUser(req.session.id);

    if (!user) {
        res.json({ error: "user not exists" });
        return;
    }

    const likedUser = req.params.id;
    db.likes.findOne({ userId: likedUser, likedUser: user._id}, (err, like) => {            
        if(like){
            db.likes.update({userId: likedUser, likedUser: user._id}, {$set:{isApproved: true}}, (err, request) => {});
            db.friends.update({userId: likedUser}, {$push: {friends: user._id}}, {upsert: true}, (err, match) =>{});
            db.friends.update({userId: user._id}, {$push: {friends: likedUser}}, {upsert: true}, (err, match) =>{});
            res.json({friends: true, msg: "friend request approved" });
        }
        else{
            db.likes.insert({ userId: user._id, likedUser, isApproved: false }, (err, docs) => {
                res.json({friends: false, msg: "friend request sent" });
            });
        }
    });
});


app.post('/register', (req, res) => {
    const body = req.body;
    const { user } = req.body;

    db.users.findOne({ user }, (err, docs) => {
        if (docs) {
            var string = "Sorry " + user + " is already exist. Try another name"
            res.json({ msg: string });
        } else {
            _.assign(body, { _id: new mongojs.ObjectId().toHexString() })
            db.users.insert(body, (err, docs) => {
                var string = user + " successfully registered"
                res.json({ msg: string });
            });
        }
    });
});


const getQueryForLocation = (coordinates, radios) => {
    const query = {
        loc: {
            $near: {
                $geometry: { type: "Point", coordinates },
                // $minDistance: 1000,
                $maxDistance: (radios || 1) * 1000,
            }
        }
    };

    return query;
}

app.use(express.static(__dirname + '/public'));

app.listen(process.env.PORT || 3000, function () {
    console.log('listening on localhost:3000');
});

db.users.ensureIndex({
                         loc: "2dsphere"
                     });